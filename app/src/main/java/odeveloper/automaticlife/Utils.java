package odeveloper.automaticlife;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.Random;

/**
 * Created by oanh.duong on 7/21/16.
 */
public class Utils {
    private static int colorIdx = 0;
    private static final int colorNums = 18;
    public static final int color_primary = Color.parseColor("#00BCD4");
    public static final int color_default = Color.parseColor("#ff3f82");

    private static int getColorIdx(){
        int result = colorIdx++;
        if(colorIdx == colorNums){
            colorIdx = 0;
        }
        return result;
    }

    public static int randomColor(Context context){
        if(context != null){
            String[] colors = context.getResources().getStringArray(R.array.colors);
            return Color.parseColor(colors[getColorIdx()]);
        }
        return Color.TRANSPARENT;
    }

    public static Drawable buildAlphabetaIcon(String title, int color, int offset){
        if(color == 0){
            color = Color.parseColor("#9E9E9E");
        }
        char offsetChar = 0;
        if(title != null && !title.isEmpty() && offset >= 0 && offset < title.length()){
            offsetChar = title.charAt(offset);
        }
        TextDrawable textDrawable = TextDrawable
                                    .builder()
                                    .buildRound(String.valueOf(offsetChar),color);
        return textDrawable;
    }
}
