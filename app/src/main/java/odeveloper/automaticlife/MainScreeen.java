package odeveloper.automaticlife;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import odeveloper.automaticlife.events.EventScreen;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MainScreeen extends AppCompatActivity {

    private static int TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_main);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent event = new Intent(MainScreeen.this, EventScreen.class);
                startActivity(event);
                finish();
            }
        },TIME_OUT);
    }
}
