package odeveloper.automaticlife.events;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import odeveloper.automaticlife.R;
import odeveloper.automaticlife.conditions.ConditionsScreen;

public class EventScreen extends AppCompatActivity {

    private ListView eventListView;
    private FloatingActionButton newEventBtn;
    private final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events_main);

        setupFloatingActionBtn();

        List<EventItem> events = new ArrayList<EventItem>();
        String[] names = new String[]{"Alarm","Charging","Music on home","Night","Music on work","Wake me up"};
        for(int i = 0; i < 6; i++){
            EventItem event = new EventItem();
            event.setTitle(names[i]);
            if(i == 1 || i == 5){
                event.setOn(true);
            }
            events.add(event);
        }
        EventListViewAdapter listViewAdapter = new EventListViewAdapter(events,this);

        eventListView = (ListView) findViewById(R.id.event_listview);
        eventListView.setAdapter(listViewAdapter);
        listViewAdapter.notifyDataSetChanged();



    }

    private void setupFloatingActionBtn() {
        newEventBtn = (FloatingActionButton) findViewById(R.id.new_event_fab);
        newEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent conditionsIntent = new Intent(context, ConditionsScreen.class);
                startActivity(conditionsIntent);
                finish();
            }
        });
    }

}
