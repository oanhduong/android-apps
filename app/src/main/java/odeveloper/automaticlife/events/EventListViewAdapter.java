package odeveloper.automaticlife.events;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import odeveloper.automaticlife.R;
import odeveloper.automaticlife.Utils;

/**
 * Created by oanh.duong on 7/20/16.
 */
public class EventListViewAdapter extends BaseAdapter {

    private List<EventItem> items;
    private Context context;

    public EventListViewAdapter(List<EventItem> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.event_row,null);
        }
        EventItem item = (EventItem) getItem(position);
        ImageView icon = (ImageView) convertView.findViewById(R.id.event_icon);

        TextView title = (TextView) convertView.findViewById(R.id.event_name);
        title.setText(item.getTitle());
        int color = position < 0 ? Utils.color_primary : Utils.color_default;
        icon.setImageDrawable(Utils.buildAlphabetaIcon(title.getText().toString(), color,0));

        Switch status = (Switch) convertView.findViewById(R.id.event_status);
        status.setChecked(item.isOn());

        return convertView;
    }
}
