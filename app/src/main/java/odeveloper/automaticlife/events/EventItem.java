package odeveloper.automaticlife.events;

/**
 * Created by oanh.duong on 7/20/16.
 */
public class EventItem {

    private String title;
    private boolean isOn;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isOn() {
        return isOn;
    }

    public void setOn(boolean on) {
        isOn = on;
    }
}
